<?php

class Users_Model extends CI_model
{

    // var $table = 'users';
    var $table = 'users'; //nama tabel dari database
    var $column_order = array('id_user', 'username', 'group'); //field yang ada di table user
    var $column_search = array('username', 'group'); //field yang diizin untuk pencarian 
    var $order = array('id_user' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if ($i === 0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    function login($username,$password)
    {

        $this->db->select('id_user, username, password, group');
        $this->db->from('users');
        //$this -> db -> where('username', $username);
        //$this -> db -> where('password', MD5($password));

        $this->db->where("username = '$username' AND password = '" .$password . "'", null, false);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function insert_upload($data)
    {
       return $this->db->insert_batch($this->table, $data);  
        // print_r($data);die;
    }
    
    function load_table($params)
    {
        $filter =    "";
        if ($params['filter'] != '') {
            $filter .= " and ( name like '%" . $params['filter'] . "%' OR username like '%" . $params['filter'] . "%' ) ";
        }
        $query = "SELECT * FROM  " . $this->table . " where 1=1 $filter  Order By id_user DESC LIMIT " . $params['offset'] . ", " . $params['limit'];

        $q = $this->db->query($query);
        return $q->result();
    }

    function total($params)
    {
        $filter =    "";
        if ($params['filter'] != '') {
            $filter .= " and name like '%" . $params['filter'] . "%'";
        }
        $query = "SELECT count(1) as total FROM  " . $this->table . " where 1=1 $filter Order By id_user Desc";

        $q = $this->db->query($query);

        return $q->result_array();
    }

    function getOne($id)
    {
        $this->db->where('id_user', $id);
        $data = $this->db->get($this->table)->result_array();
        return $data;
    }

    function getUserBranch($id)
    {
        $this->db->select('id_branch');
        $this->db->where('id_user', $id);
        $this->db->group_by('id_branch');
        $data = $this->db->get($this->table . '_branch')->result_array();
        return $data;
    }

    function getUserRegion($id)
    {
        $this->db->select('id_region');
        $this->db->where('id_user', $id);
        $this->db->group_by('id_region');
        $data = $this->db->get($this->table . '_region')->result_array();
        return $data;
    }

    function getUserSalesman($id)
    {
        $this->db->select('id_salesman');
        $this->db->where('id_user', $id);
        $this->db->group_by('id_salesman');
        $data = $this->db->get($this->table . '_salesman')->result_array();
        return $data;
    }

    function getUserSubbranch($id)
    {
        $this->db->select('id_subbranch');
        $this->db->where('id_user', $id);
        $this->db->group_by('id_subbranch');
        $data = $this->db->get($this->table . '_subbranch')->result_array();
        return $data;
    }

    function cektreeup($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', '1');
        $data = $this->db->get('menu')->result_array();

        //GALIH FIXING undefined xx 
        //START
        $xx['id'] = '';
        $xx['parent'] = '';
        //GALIH END

        foreach ($data as $item) {
            $xx['id'] = $item['id'];
            $xx['parent'] = $item['parent'];
        }
        return $xx;
    }

    function cektreedown($parent)
    {
        $this->db->where('parent', $parent);
        $this->db->where('status', '1');
        $data = $this->db->get('menu')->result_array();
        $x = 0;
        foreach ($data as $item) {
            $xx[$x] = $item['id'];
            $x++;
        }
        return $xx;
    }

    function delete($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->delete($this->table)) {
            $this->delsalesman($id);
            $this->delsubbranch($id);
            $this->delbranch($id);
            $this->delregion($id);
            $result = array('message' => 'success delete');
        } else {
            $result = array('message' => 'Failed delete');
        }

        return $result;
    }

    function delsalesman($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->delete('users_salesman')) {
            //$result = array('message'=>'success delete');
        } else {
            //$result = array('message'=>'Failed delete');
        }

        //return $result;
    }
    function delsubbranch($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->delete('users_subbranch')) {
            //$result = array('message'=>'success delete');
        } else {
            //	$result = array('message'=>'Failed delete');
        }

        //	return $result;
    }

    function delbranch($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->delete('users_branch')) {
            //$result = array('message'=>'success delete');
        } else {
            //	$result = array('message'=>'Failed delete');
        }

        //return $result;
    }

    function delregion($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->delete('users_region')) {
            //	$result = array('message'=>'success delete');
        } else {
            //	$result = array('message'=>'Failed delete');
        }

        //	return $result;
    }

    //	function save($id, $username, $password, $name, $email, $level, $role){
    function save($id, $username, $password, $name, $email, $level)
    {
        $this->db->set('username', $username);
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('allow_menu', $level);
        //		$this->db->set('role', $role );

        if (!empty($id)) {

            if (!empty($password)) {
                $this->db->set('password', ($password));
            }
            $this->db->where('id_user', $id);
            if ($this->db->update($this->table)) {
                $result = array('message' => 'success update data');
            } else {
                $result = array('message' => 'Failed update data');
            }
        } else {

            $this->db->set('password', ($password));

            if ($this->db->insert($this->table)) {
                $result = array('message' => 'success insert data');
            } else {
                $result = array('message' => 'Failed insert data');
            }
        }

        return $result;
    }

    function getuserId($username, $name, $email)
    {
        $query = "SELECT id_user 
				from users 
				where 
				username 	= LEFT('$username',10) AND 
				name		= '$name' AND 
				email		= '$email'";

        $q = $this->db->query($query);

        if ($q->result_array()) {
            $result['status'] = 'sukses';
            $result['data'] = $q->result_array();
        } else {
            $result['status'] = 'gagal';
        }
        return $result;
    }

    function delcategory($id_user, $table)
    {
        $this->db->where('id_user', $id_user);
        $this->db->delete($table);
    }

    function savecategory($id_user, $val_category, $kolom, $tabel)
    {
        $this->db->set('id_user', $id_user);
        $this->db->set($kolom, $val_category);

        if ($this->db->insert($tabel)) {
            $result['savecat'] = 1;
        } else {
            $result['savecat'] = 0;
        }
        return $result;
    }

    function saveaccess($id_user, $id_menu, $accesstext)
    {
        $table = "users_level";
        $this->db->set('id_user', $id_user);
        $this->db->set('id_menu', $id_menu);
        $this->db->set('access', $accesstext);

        if ($this->db->insert($table)) {
            $result['savecat'] = 1;
        } else {
            $result['savecat'] = 0;
        }
        return $result;
    }

    function _load_menu()
    {
        $query = "SELECT * from menu where status = 1";
        $q = $this->db->query($query);
        return $q->result_array();
    }

    function export($file_excel)
    {
        // retrive data which you want to export
        $query     = "SELECT id_user, username,name,email,lastlogin,ip FROM " . $this->table;
        $export = mysql_query($query);
        $this->toExcelFile($file_excel, $export);
    }
}
