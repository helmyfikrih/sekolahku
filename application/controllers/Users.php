<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Users extends CI_Controller
{

    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $session_data = $this->session->userdata('logged_in');

        if (empty($session_data)) {
            redirect('login', 'refresh');
        }
        $this->data['username'] = $session_data['username'];
        $this->data['user_id'] = $session_data['id'];

        $this->data['domain'] = base_url();
        $this->data['page_title'] = 'Utility - User';
        $this->data['desc'] = 'User';
        $this->data['css_alert'] = 'alert-success hide';
        $this->data['title'] = 'Data Users';

        #Menu Body
        $this->load->model('menu_model', 'menu_model');
        $menu_body     = $this->menu_model->getmenu($session_data['id'], $session_data['username']);
        $this->data['menu_body'] = $menu_body;

        $urlname    = strtolower($this->router->fetch_class());
        $menu_id       = $this->menu_model->idMenu($urlname);
        $user_allow = $this->menu_model->UserAllow($session_data['id']);
        $user_allow_menu = explode(",", $user_allow[0]['allow_menu']);
        $this->data['menu_allow'] = '';
        $this->data['user_allow_menu'] = $user_allow_menu;
        // print_r( $session_data['group'] != ADMINGROUP);die;
        if ($session_data['group'] != ADMINGROUP) {
            if (@in_array($menu_id[0]['id'], $user_allow_menu)) {
                $this->data['menu_allow'] = 'level_' . $menu_id[0]['id'];
            } else {
                redirect('login', 'refresh');
            }
        }
        ####

        $this->data['menuakses'] = $this->getmenulist();
    }

    function index()
    {
        $this->template->load('default', 'v_users', $this->data);
    }

    function get_data_user()
    {
        $list = $this->users_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $button = "<center><a class='btn btn-danger btn-xs text-center'> <i class='fa fa-trash-o'></i> Delete<a>
                        <a href='javascript:;' class='btn btn-info btn-xs text-center' onClick='editData(\"$field->id_user\")'> <i class='fa fa-pencil'></i> Edit<a></center>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->username;
            $row[] = $field->group;
            $row[] = $button;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->users_model->count_all(),
            "recordsFiltered" => $this->users_model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }


    function uploadUsers()
    {
        $invalid		= 0;
		$valid			= 0;
		$exist			= 0;
		$msg 			= ""; 
		$arrSucesss		= array();
		$arrDuplicate	= array();
        $arrUpdate	= array();
        $data = array();
        set_time_limit(0);
       
        if (!empty($_FILES['namafile']) && isset($_FILES['namafile']) && $_FILES['namafile']['size'] < 10485760) {

            $tmp       = explode('.', $_FILES['namafile']['name']);
            $extension = end($tmp);
            
            if ($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            $uniqid     = date("Ymd") . date("His");
            $filename  = $_FILES['namafile']['name'];
            $filetemp  = $_FILES['namafile']['tmp_name'];
            $file_code_outlet       = "promo_target_". $uniqid;

            $target_dir         = "./assets/uploaded_files/";
            $dir                 = "assets/uploaded_files/";
            $target_file         = $target_dir . basename($_FILES["namafile"]["name"]);
            $target_file_new     = pathinfo($target_file);
            $new_file             = $target_dir . $file_code_outlet . "." . $target_file_new['extension'];

            if (move_uploaded_file($_FILES["namafile"]["tmp_name"], $new_file)) {
                //echo "The file moved";
                $file_dir = $dir . $file_code_outlet . "." . $target_file_new['extension'];
            } else {
                $msg = "The file unable to moved";
                $invalid++;
            }
           
            // file path
            $spreadsheet = $reader->load($file_dir);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $n = 0;
            $jml = 0;
            $i = 0;
            
            foreach ($allDataInSheet as $row) {
                if ($n != 0) {
                    // $validData = true;
                    

                    if (
                        !empty($row['A']) &&
                        !empty($row['B'])
                    ) {

                        $username     = trim($row['A']);
                        $password     = trim($row['B']);
                        $name         = trim($row['C']);
                        $email        = trim($row['D']);
                        // $data = array(
                        //     'username' => $username,
                        //     'password' => $password,
                        //     'name' => $name   ,
                        //     'email' =>  $email 
                        // );
                        
                        $data[$n]['username'] = $username;
                        $data[$n]['password'] = $password;
                        $data[$n]['name'] = $name;
                        $data[$n]['email'] = $email;
                        $data[$n]['allow_menu'] = 1;
                        // print_r($username."_". $password."_". $name."_". $email. "\n");
                       
                    }
                }
                $n++;
            }
          
         }
        $this->users_model->insert_upload($data);

        $msg = "Sucess Upload!";
        $message = array(
            'message' => '3',
            'icon' => 'success',
            'title' => " Sucess!",
            'messages' => $msg
        );
        // print_r($data);
        // die;
        // $message = array(
        //     'message' => '3',
        //     'icon' => 'error',
        //     'title' => " Gagal No File Uploaded!",
        //     'messages' => "<div style='text-align:center; font-size:120%; font-style:italic; margin-top:-2vh;;'></div><br/>
		// 			<div style='background-color: #FEFAE3;padding: 5px; border: 1px solid #F0E1A1;display: block; margin: 5px; text-align: left; color: black; font-size:85%;'></div>
		// 			<p style='text-align:center; font-size:110%; color:red; margin-top:2vh;'></p>"
        // );
        echo json_encode($message);
    }

    function load_table()
    {

        $limit  = 10;
        $page   = $this->uri->segment(3);

        // echo $page.'page';

        $offset = (isset($page)) ? (int) $page : 0;
        $params = array(
            'offset' => $offset,
            'limit'  => $limit,
            'filter' => $this->input->post('txt_src')
        );
        $total  = $this->sales->total($params);

        $result = $this->sales->load_table($params, '');

        $config['base_url']    = BASE_URL . 'user/load_table/';
        $config['total_rows']  = $total[0]['total'];
        $config['per_page']    = $limit;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        $data['total']      = $total;
        //$data['result']     = $result;
        $data['result'] = $this->getHTMLtable($result, $params);
        $data['pagination'] = $this->pagination->create_links();
        echo json_encode($data);
    }

    function getHTMLtable($result, $params)
    {

        $bodytable = '';
        $session_data = $this->session->userdata('logged_in');

        foreach ($result as $value) {
            $edit_butt = '';
            $del_butt  = '';
            $action     = '';
            $bodytable     .= '<tr class="odd gradeX">';
            $bodytable     .= '<td>' . $value->id_user . '</td>';
            $bodytable     .= '<td>' . $value->username . '</td>';
            $bodytable     .= '<td>' . $value->name . '</td>';
            $bodytable     .= '<td>' . $value->email . '</td>';
            $bodytable     .= '<td>' . $value->lastlogin . '</td>';
            $bodytable     .= '<td>' . $value->ip . '</td>';
            if ($value->username != 'admin') {
                if (($session_data['group'] != ADMINGROUP)) {
                    if (in_array($this->data['menu_allow'] . '_edit', $this->data['user_allow_menu'])) {
                        $edit_butt     = '<a href="javascript:;" onClick="editData(\'' . $value->id_user . '\')" style="padding:0px 8px;" class="btn green">Edit</a>';
                    }
                    if (in_array($this->data['menu_allow'] . '_delete', $this->data['user_allow_menu'])) {
                        $del_butt    = '<a href="javascript:;" style="padding:0px 8px;" class="btn red icn-only" onClick="deleteData(\'' . $value->id_user . '\')">Delete</a>';
                    }
                } else {
                    $edit_butt     = '<a href="javascript:;" onClick="editData(\'' . $value->id_user . '\')" style="padding:0px 8px;" class="btn green">Edit</a>';
                    $del_butt    = '<a href="javascript:;" style="padding:0px 8px;" class="btn red icn-only" onClick="deleteData(\'' . $value->id_user . '\')">Delete</a>';
                }
            }
            $bodytable     .= '<td><center>' . $edit_butt . ' ' . $del_butt . '</center></td>';
            $bodytable     .= '</tr>';
        }

        return $bodytable;
    }


    function delete()
    {
        if ($_POST) {
            $id     = $this->input->post('id');
            $result = $this->sales->delete($id);
            echo json_encode($result);
            log_message('error', 'Delete Route : ' . $id . 'by ' . $this->data['username']);
        }
    }

    function getOne()
    {
        $id     = $this->input->post('id');
        $result = $this->users_model->getOne($id);
        // $result['branch'] = $this->sales->getUserBranch($id);
        // $result['Region'] = $this->sales->getUserRegion($id);
        // $result['salesman'] = $this->sales->getUserSalesman($id);
        // $result['subbranch'] = $this->sales->getUserSubbranch($id);

        echo json_encode($result);
    }

    function save()
    {
        if ($_POST) {
            $id         = $this->input->post('id');
            $username   = $this->input->post('username');
            $password   = $this->input->post('password');
            $name       = $this->input->post('name');
            $email         = $this->input->post('email');
            $allow_menu = $this->input->post('allow_menu');
            //			$role 	    = $this->input->post('role');

            $result     = $this->sales->save($id, $username, $password, $name, $email, $allow_menu);
            //			$result     = $this->sales->save($id, $username, $password, $name, $email, $allow_menu, $role);

            echo json_encode($result);
        }
    }

    #SAVING DATA
    function save_user()
    {   
        // print_r($_POST);die;
        if ($_POST) {
            $id         = $this->input->post('id');
            $username   = $this->input->post('username');
            $password   = $this->input->post('password');
            $name       = $this->input->post('name');
            $email      = $this->input->post('email');
            $level      = $this->input->post('level');
            //			$role 	    = $this->input->post('role');

            if (empty($level)) {
                $level = 1;
            }

            // $region     = $this->input->post('region');
            // $branch     = $this->input->post('branch');
            // $subbranch  = $this->input->post('subbranch');
            // $salesman   = $this->input->post('salesman');

            //			$simpandetail     = $this->sales->save($id, $username, $password, $name, $email, $level, $role);
            $simpandetail     = $this->users_model->save($id, $username, $password, $name, $email, $level);
            // $iduser           = $this->sales->getuserId($username,  $name, $email);
            // //print_r($iduser);
            // if (empty($id)) {
            //     $id_user   = $iduser['data'][0]['id_user'];
            // } else {
            //     $id_user   = $id;
            // }

            // if (isset($id_user)) {

            //     $this->sales->delcategory($id_user, 'users_region');
            //     $this->sales->delcategory($id_user, 'users_branch');
            //     $this->sales->delcategory($id_user, 'users_subbranch');
            //     $this->sales->delcategory($id_user, 'users_salesman');
            //     $this->sales->delcategory($id_user, 'users_level');

            //     $arr_level = explode(",", $level);
            //     foreach ($arr_level as $arr_value) {
            //         if (substr($arr_value, 0, 5) == 'level') {
            //             $arr_akses = explode("_", $arr_value);
            //             $this->savaccess($id_user, $arr_akses[1], $arr_akses[2]);
            //         }
            //     }

            //     if ($region) {
            //         foreach ($region as $item) {
            //             $this->savecat($id_user, $item, 'id_region', 'users_region');
            //         }
            //     } else {
            //         $this->savecat($id_user, 0, 'id_region', 'users_region');
            //     }

            //     if ($branch) {
            //         foreach ($branch as $item) {
            //             $this->savecat($id_user, $item, 'id_branch', 'users_branch');
            //         }
            //     } else {
            //         $this->savecat($id_user, 0, 'id_branch', 'users_branch');
            //     }

            //     if ($subbranch) {
            //         foreach ($subbranch as $item) {
            //             $this->savecat($id_user, $item, 'id_subbranch', 'users_subbranch');
            //         }
            //     } else {
            //         $this->savecat($id_user, 0, 'id_subbranch', 'users_subbranch');
            //     }

            //     if ($salesman) {
            //         foreach ($salesman as $item) {
            //             $this->savecat($id_user, $item, 'id_salesman', 'users_salesman');
            //         }
            //     } else {
            //         $this->savecat($id_user, 0, 'id_salesman', 'users_salesman');
            //     }
            // }

            //$result     = $this->sales->save_user_globe($id, $username, $password, $name, $email, $region, $branch, $subbranch, $salesman, $level);

            echo json_encode($simpandetail);
        }
    }

    function getUserId($username, $name, $email)
    {
        $this->load->model('user_model', 'user');
        $menu = array();
        $menu_top = $this->user->getuserId($username, $name, $email);
        return $menu_top;
        //echo  $menu_top['data'][0]['id_user'];
    }

    function savecat($id_user, $value, $kolom, $tabel)
    {
        $this->load->model('user_model', 'user');
        $data = $this->user->savecategory($id_user, $value, $kolom, $tabel);
        return $data;
    }

    function savaccess($id_user, $id_menu, $accesstext)
    {
        $this->load->model('user_model', 'user');
        $data = $this->user->saveaccess($id_user, $id_menu, $accesstext);
        return $data;
    }

    function getmenulist()
    {
        $this->load->model('menu_model', 'menu_model');
        $menu = array();
        $menu_top = $this->menu_model->getmenujson();
        return $menu_top;
    }

    function cekpohon()
    {
        $id = $this->input->post('id');
        $this->load->model('users_model', 'user');
        $menu = array();
        $pohon = $this->user->cektreeup($id);
        //echo json_encode($pohon);exit;
        $x = $pohon['parent'];
        //$menu[]=$pohon;

        while ($x > 0) {
            $coba = $this->user->cektreeup($x);
            $menu[] = $coba;
            $x = $coba['parent'];
        }
        //print_r($menu);exit;
        echo  json_encode($menu);
    }

    function getmenulist2()
    {
        $this->load->model('menu_model', 'menu_model');
        $menu = array();
        $menu_top = $this->menu_model->getmenujson();
        //echo json_encode($menu_top);

        define('MAINDOMAIN', 'peka');
        $data = $menu_top;
        foreach ($data['root'] as $top) {
            $rootname = $top['name'];
            $rootid   = $top['id'];

            if (isset($data['sub_' . $rootid])) {
                echo '<li>
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="' . MAINDOMAIN . '">' . $rootname . '
                                        <span class="arrow"></span></a>
                                        <ul class="dropdown-menu">';
                foreach ($data['sub_' . $rootid] as $sub_satu) {

                    if (isset($data['sub_' . $rootid . '_' . $sub_satu->id])) {
                        echo '<li class="dropdown-submenu">
                                               <a href="javascript:;">' . $sub_satu->name . '<span class="arrow"></span></a>
                                            <ul class="dropdown-menu">';

                        foreach ($data['sub_' . $rootid . '_' . $sub_satu->id] as $sub_dua) {
                            echo '<li><a href="' . MAINDOMAIN . $sub_dua->url . '">' . $sub_dua->name . '</a></li>';
                        }
                        echo '</ul>
                                           </li>';
                    } else {
                        echo '<li><a href="' . MAINDOMAIN . $sub_satu->url . '">' . $sub_satu->name . '</a></li>';
                    }
                }
                echo ' </ul>
                                        <b class="caret-out"></b>                        
                                     </li>';
            } else {
                echo '<li><a <a href="' . MAINDOMAIN . $top['url'] . '">' . $rootname . '</a></li>';
            }
        }
    }

    function get_list()
    {
        $id = $this->input->post('id');
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->routing($id);
        echo json_encode($list);
    }

    function load_region()
    {
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->_load_region();
        echo json_encode($list);
    }

    function load_branch_old()
    {
        $id_region = $this->input->post('id_region');
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->_load_branch($id_region);
        echo json_encode($list);
    }

    function load_branch()
    {
        $id_subbranch     = $this->input->post('id_subbranch');
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->_load_branch($id_subbranch);
        echo json_encode($list);
    }

    function load_subbranch()
    {
        $id_region = $this->input->post('id_region');
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->_load_subbranch($id_region);
        echo json_encode($list);
    }

    function load_salesman()
    {
        $id_subbranch = $this->input->post('id_subbranch');
        $this->load->model('menu_model', 'menu_model');
        $list = $this->menu_model->_load_salesman($id_subbranch);
        echo json_encode($list);
    }

    function prepareExport()
    {
        $this->load->model('user_model', 'user_model');
        $result = $this->user_model->export('Data_User');
    }
}
