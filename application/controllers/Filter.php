<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filter extends CI_Controller {
    
    function getProvinsi()
    {
        $result = $this->filter_model->getProvinsi();
        echo json_encode($result);
    }

    function getKabupaten()
    {
        $idProvinsi = "";
        $idProvinsi = $this->input->post('idProvinsi');
        $result = $this->filter_model->getKabupaten($idProvinsi);
        echo json_encode($result);
    }

    function getKecamatan()
    {
        $idKabupaten = "";
        $idKabupaten = $this->input->post('idKabupaten');
        $result = $this->filter_model->getKecamatan($idKabupaten);
        echo json_encode($result);
    }

    function getDesa()
    {
        $idKecamatan = "";
        $idKecamatan = $this->input->post('idKecamatan');
        $result = $this->filter_model->getDesa($idKecamatan);
        echo json_encode($result);
    }

    function getTahunAjaran()
    {
        $result = $this->filter_model->getTahunAjaran();
        echo json_encode($result);
    }

    function checkCode()
    {
        $code         = $this->input->post('code');
        $field         = $this->input->post('field');
        $table         = $this->input->post('table');
        $result     = $this->filter_model->checkCode($code, $field, $table);
        echo $result[0]['total'];
    }

}