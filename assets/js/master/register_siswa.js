function valid_step1(){
    var valid = true;
    var radioValue = $("input[name='jenis_kelamin']:checked").val();
    if (!radioValue) {
        $("#jenis_kelamin").addClass('makeRed');
        valid = false;
    } else {
        $("#jenis_kelamin").removeClass('makeRed');
    }
    $('form').find('input').each(function () {
        if ($(this).prop('required') && ($(this).val() == '')) {
            // $(this).parent().find('.bar').addClass('red-bar');
            $(this).addClass('makeRed');
            valid = false;
        } else {
            $(this).removeClass('makeRed');
        }
    });
    return valid;
}


function valid_step2() {
    return true;
}