<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filter_model extends CI_Model {

    function getProvinsi()
    {
        $this->db->from('data_provinsi');
        return $this->db->get()->result_array();
    }

    function getKabupaten($idProvinsi)
    {
        $this->db->from('data_kabupaten');
        $this->db->where('id_provinsi',$idProvinsi);
        return $this->db->get()->result_array();
    }

    function getKecamatan($idKabupaten)
    {
        $this->db->from('data_kecamatan');
        $this->db->where('id_kabupaten', $idKabupaten);
        return $this->db->get()->result_array();
    }

    function getDesa($idKecamatan)
    {
        $this->db->from('data_desa');
        $this->db->where('id_kecamatan', $idKecamatan);
        return $this->db->get()->result_array();
    }
    
    function getTahunAjaran()
    {
        $this->db->from('data_tahun_akademik');
        return $this->db->get()->result_array();
    }

    function checkCode($code, $field, $table)
    {
        $query = "SELECT COUNT(1) AS total  
				FROM  " . $table . " 
				WHERE 
				$field = '$code'";

        $q = $this->db->query($query);
        return $q->result_array();
    }
}