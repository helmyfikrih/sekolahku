<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tahun_akademik extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $session_data = $this->session->userdata('logged_in');

        if (empty($session_data)) {
            redirect('login', 'refresh');
        }
        //Do your magic here
        $this->load->model('m_tahun_akademik', 'TA');
        $this->data['domain'] = base_url();
        $this->data['title'] = 'Tahun Akademik';
        $menu_body     = $this->menu_model->getmenu($session_data['id'], $session_data['username']);
        $this->data['menu_body'] = $menu_body;
    }


    public function index()
    {
        $this->template->load('default', 'v_tahun_akademik', $this->data);
    }

    function getTahunAkademik()
    {
        $list = $this->TA->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $button = "<center><div class='col-md-3 col-sm-6'><a class='btn btn-danger btn-xs text-center'> <i class='fa fa-trash-o'></i> Delete<a></div>
                        <div class='col-md-3 col-sm-6'><a href='javascript:;' class='btn btn-info btn-xs text-center' onClick='editData(\"$field->id_ta\")'> <i class='fa fa-pencil'></i> Edit<a></div>
                        </center>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->tahun_awal;
            $row[] = $field->tahun_akhir;
            $row[] = ($field->semester == '1' ? 'Ganjil' : 'Genap');
            $row[] = $button;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->TA->count_all(),
            "recordsFiltered" => $this->TA->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }

}

/* End of file Home.php */
