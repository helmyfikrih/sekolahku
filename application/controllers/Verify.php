<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Verify extends CI_Controller {
	
	 function __construct()
	 {
	   parent::__construct();
	 }

	 function index() {
		// $code = $this->input->post('code');
	   
	    // if( (strtolower($code) != strtolower($_SESSION['random_number'])) OR empty($_SESSION['random_number']) ) {
		// 	redirect(BASE_URL.'login?w=2', 'refresh');
		// }
		 
	   //This method will have the credentials validation
	   $this->load->library('form_validation');

	   $this->form_validation->set_rules('username', 'Username', 'trim|required');
	   $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

	   if($this->form_validation->run() == FALSE) {
			redirect('login?w=1', 'refresh');
	   } else {
			redirect('home', 'refresh');
	   }

	 }
	 
	 function check_database($password) {
		   //Field validation succeeded.  Validate against database
		   $username = $this->input->post('username');
		   //query the database
		   $result = $this->users_model->login($username, $password);

		   if($result) {
				 $sess_array 	= array();
				 
				 foreach($result as $row) {
				   $sess_array = array(
					 'id' 			=> $row->id_user,
					 'username' 	=> $row->username,
					 'group'		=> $row->group
				   );
				   $this->session->set_userdata('logged_in', $sess_array);
				 }
				 
				 return TRUE;
				 
		   } else {
				 $this->form_validation->set_message('check_database', 'Invalid username or password');
				 return false;
		   }
		   
	 }
 
 
 
}
