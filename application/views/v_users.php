<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Blank page
      <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">Blank page</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Title</h3>

        <div class="box-tools pull-right">
          <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button> -->
          <button type="button" class="btn btn-box-tool" id="add-group" onclick="clearData()" title=" Add New User">
            <i class="fa fa-plus"></i><span> USER</span></button>
          <button class="btn btn-box-tool" data-toggle="dropdown" href="#">
            <i class="fa fa-upload"></i> UPLOAD
          </button>
          <ul class="dropdown-menu">
            <li><a href="assets/template/outlet_realisasi_target_promo.xls"><i class="fa fa-download"></i> Download Template&nbsp;&nbsp;</a></li>
            <!-- <li><a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modal-upload" onclick="Upload()"><i class="icon-upload"></i> Upload&nbsp;&nbsp;</a></li> -->
            <li><a onclick="Upload();"><i class="fa fa-upload"></i> Upload&nbsp;&nbsp;</a></li>
          </ul>
          <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button> -->
        </div>
      </div>
      <div class="box-body">

        <div class="toggle-add-group" style="display:none; background-color:#EEEEEE;width:800px;min-height:100px;position:absolute;z-index:9; padding:10px; margin-top:-10px">
          <style>
            .row {
              margin-left: -15px;
              margin-right: -15px;
            }

            .col-md-6 {
              width: 45%;
              position: relative;
              min-height: 1px;
              padding-left: 15px;
              padding-right: 15px;
              float: left;
            }
          </style>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <table border="0" cellspacing="10" cellpadding="10" width="100%">
                <input type="hidden" id="txt_id" name="txt_id" />
                <tr>
                  <td>Username</td>
                  <td><input type="text" placeholder="User Name" id="username" name="username" class="form-control" /></td>
                </tr>
                <tr>
                  <td>Password</td>
                  <td><input type="text" placeholder="password" id="password" name="password" class="form-control" /></td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td><input type="text" placeholder="name" id="name" name="name" class="form-control" /></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><input type="text" placeholder="email" id="email" name="email" class="form-control" /></td>
                </tr>
                <!-- <tr>
                  <td valign="top">Region</td>
                  <td>
                    <div id="selector"><select multiple="region" name="region" id="id_region" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Distributor</td>
                  <td>
                    <div id="selector"><select multiple="subbranch" name="subbranch" id="id_subbranch" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Territory</td>
                  <td>
                    <div id="selector"><select multiple="branch" name="branch" id="id_branch" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Salesman</td>
                  <td>
                    <div id="selector"><select multiple="salesman" name="salesman" id="id_salesman" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr> -->
                <tr>
                  <td></td>
                  <td>
                    <button name="" id="" class="btn btn-info" onclick="input_user_data()">Submit</button>&nbsp;
                    <button name="" id="" class="btn btn-info" onclick="remove_toggle('toggle-add-group'); clearData();">Cancel</button>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-md-6 col-sm-6">
              <style>
                .akar_dua {
                  padding-left: 25px
                }

                .akar_tiga {
                  padding-left: 50px
                }

                #accordion3 input[type="checkbox"] {
                  margin-top: -3px !important;
                }

                #selector {
                  width: 220px;
                  overflow: auto;
                  margin-bottom: 10px;
                }

                .scroll-select {
                  width: auto !important;
                  min-width: 220px;
                  margin-bottom: 0px !important;
                }
              </style>

              <!-- <div id="iregion" style="display:none"></div>
                    <div id="ibranch" style="display:none"></div>
                    <div id="isubbranch" style="display:none"></div>
                    <div id="isalesman" style="display:none"></div> -->
              <div id="imenu" style="display:none"></div>
              <div class="panel-group accordion" id="accordion3">
                Access Level
                <hr />
                <?php
                define('MAINDOMAIN', 'peka');
                $data = $menuakses;
                // print_r($data);
                foreach ($data['root'] as $top) {
                  $rootname = $top['name'];
                  $rootid   = $top['id'];

                  if (isset($data['sub_' . $rootid])) {
                    echo '<div class=""><img src="assets/img/text-plus-icon.png" width="12px" style="margin-top:-1px"> 
									<input id="box_' . $rootid . '" type="checkbox" value="' . $rootid . '" onclick="treedata(this)" >
									<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_' . $rootid . '">' . $rootname . '</a>
								  <div style="height: 0px; " id="collapse_' . $rootid . '" class="panel-collapse collapse akar_dua down_' . $rootid . '">';

                    foreach ($data['sub_' . $rootid] as $sub_satu) {

                      if (isset($data['sub_' . $rootid . '_' . $sub_satu->id])) {
                        echo '<div class=""><img src="assets/img/text-plus-icon.png" width="12px" style="margin-top:-2px"> 
											<input type="checkbox" id="box_' . $sub_satu->id . '" value="' . $sub_satu->id . '" >
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#sub_tiga_' . $sub_satu->id . '">' . $sub_satu->name . ' <span class="arrow"></span></a>
											<div style="height: 0px;" id="sub_tiga_' . $sub_satu->id . '" class="panel-collapse collapse akar_dua down_' . $sub_satu->id . '">';

                        foreach ($data['sub_' . $rootid . '_' . $sub_satu->id] as $sub_dua) {

                          if (isset($data['sub_' . $rootid . '_' . $sub_satu->id . '_' . $sub_dua->id])) {
                            echo '<div class=""><img src="assets/img/text-plus-icon.png" width="12px" style="margin-top:-2px"> 
													<input type="checkbox" id="box_' . $sub_dua->id . '" value="' . $sub_dua->id . '" >
												    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#sub_tiga_' . $sub_dua->id . '">' . $sub_dua->name . ' <span class="arrow"></span></a>
													<div style="height: 0px;" id="sub_tiga_' . $sub_dua->id . '" class="panel-collapse collapse akar_dua down_' . $sub_dua->id . '">';

                            foreach ($data['sub_' . $rootid . '_' . $sub_satu->id . '_' . $sub_dua->id] as $sub_tiga) {
                              echo '<input style="margin-left: 15px;" type="checkbox" id="box_' . $sub_tiga->id . '" value="' . $sub_tiga->id . '" onclick="treedata(this)"> ' . $sub_tiga->name . '<br>';
                              if (!empty($sub_tiga->access)) {
                                echo '<div style="height: auto;" id="collapse_' . $sub_tiga->id . '" class="panel-collapse akar_dua down_' . $sub_tiga->id . ' in collapse">';
                                $arr_level = explode(",", $sub_tiga->access);
                                foreach ($arr_level as $arr_value) {
                                  $level_value = 'level_' . $sub_tiga->id . '_' . $arr_value;
                                  echo '<input style="margin-left: 15px;" style="margin-left: 26px;" type="checkbox" id="box_' . $level_value . '" value="' . $level_value . '" onclick="treedata(this)"> ' . $arr_value . '<br>';
                                }
                                echo '</div>';
                              }
                            }
                            echo '</div></div>';
                          } else {
                            echo '<input style="margin-left: 15px;" type="checkbox" id="box_' . $sub_dua->id . '" value="' . $sub_dua->id . '" onclick="treedata(this)"> ' . $sub_dua->name . '<br>';
                            if (!empty($sub_dua->access)) {
                              echo '<div style="height: auto;" id="collapse_' . $sub_dua->id . '" class="panel-collapse akar_dua down_' . $sub_dua->id . ' in collapse">';
                              $arr_level = explode(",", $sub_dua->access);
                              foreach ($arr_level as $arr_value) {
                                $level_value = 'level_' . $sub_dua->id . '_' . $arr_value;
                                echo '<input style="margin-left: 15px;" type="checkbox" id="box_' . $level_value . '" value="' . $level_value . '" onclick="treedata(this)"> ' . $arr_value . '<br>';
                              }
                              echo '</div>';
                            }
                          }
                        }
                        echo '</div></div>';
                      } else {
                        echo '<input style="margin-left: 15px;" type="checkbox" id="box_' . $sub_satu->id . '" value="' . $sub_satu->id . '" onclick="treedata(this)" > ' . $sub_satu->name . '<br>';
                        if (!empty($sub_satu->access)) {
                          $arr_level = explode(",", $sub_satu->access);
                          echo '<div style="height: auto;" id="collapse_' . $sub_satu->id . '" class="panel-collapse akar_dua down_' . $sub_satu->id . ' in collapse">';
                          foreach ($arr_level as $arr_value) {
                            $level_value = 'level_' . $sub_satu->id . '_' . $arr_value;
                            echo '<input style="margin-left: 15px;" type="checkbox"  id="box_' . $level_value . '"  value="' . $level_value . '" onclick="treedata(this)"> ' . $arr_value . '<br>';
                          }
                          echo '</div>';
                        }
                      }
                    }
                    echo ' </div>
                				</div>';
                  } else {
                    echo '<div class="" style="margin-left: 15px;"><input id="box_' . $rootid . '" type="checkbox" value="' . $rootid . '" onclick="treedata(this)" ><a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_' . $rootid . '"> ' . $rootname . '</a></div>';
                  }
                }

                ?>
              </div>
            </div>
          </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Group</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Group</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="modal fade" id="modal-upload">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body">
              <!-- <p>Select File To Upload!</p> -->
              <!-- <div id="fileuploader">Upload</div> -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="upload_file">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <div class="box-footer">
        Footer
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Upload -->
<div id="dialog_upload" title="UPLOAD">
  <div id="uploader">
  </div>
</div>
<div id="upload_confirm" class="upload_confirm" style="display:none" title="Upload this file?">
  <p><span class="icon icon-warning-sign"></span>
    Are you sure, to Upload This File?</p>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- <script src="<?= $domain ?>assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/data-tables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/plugins/bootstrap-tree/bootstrap-tree/js/bootstrap-tree.js" type="text/javascript"></script> -->

<script src="<?= $domain ?>assets/js/master/filter.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/js/master/users.js" type="text/javascript"></script>
<!-- jQuery -->
<!-- <script src="<?= $domain ?>assets/template/vendors/jquery/dist/jquery.min.js"></script> -->


<!-- END PAGE LEVEL PLUGINS -->

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
  var domain = '<?= $domain ?>';
</script>


<script type="text/javascript">
  var table;
  $(document).ready(function() {
    var buttonCommon = {
      exportOptions: {
        format: {
          body: function(data, row, column, node) {
            // Strip $ from salary column to make it numeric
            return column === 5 ?
              data.replace(/[$,]/g, '') :
              data;
          }
        }
      }
    };

    //datatables
    loadTable();

    //LOAD FOR INPUT/EDIT


    $('#id_region').change(function() {
      $('#id_subbranch option').remove();
      $('#id_branch option').remove();
      loadSubbranchMulti($('#id_region').val(), '');
    });

    $('#id_subbranch').change(function() {
      $('#id_branch option').remove();
      loadBranchMulti($('#id_subbranch').val(), '');
      loadSalesmanMultiOrderWeb($('#id_subbranch').val(), '');
    });

    // $('#id_branch').change(function(){
    // $('#id_salesman option').remove();		
    // loadSalesmanMulti( $('#id_region').val(), $('#id_branch').val(),$('#id_subbranch').val(), '');	
    // });

    //load_region();
    // loading('tab-content');

    // $('#paging a').live('click', function() {
    //   var data = {};
    //   load_table_sales_cost($(this).attr('href'), data);
    //   return false;
    // });

    $('#add-group').click(function() {
      clearData()
      $("#box_1").attr("checked", "checked");
      $("#password").attr("placeholder", "password");
      // loadRegionMulti('');
      $('.toggle-add-group').toggle('slow');
      $('table').css({
        'border-spacing': 'none',
        'border-collapse': 'none'
      })
      return false;
    });
  });
</script>