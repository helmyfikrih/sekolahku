function getProvinsi() {
    $.ajax({
        type: 'post',
        url: domain + "filter/getProvinsi",
        dataType: 'json',
        success: function (data) {
            var opt;
            opt = "<option value='0'>- Select Provinsi -</option>";
            $.each(data, function (k, v) {
                opt += "<option value=" + v.id + " >" + v.name + "</option>";
            });

            $("#provinsi").append(opt);
        }

    });
}

function getKabupaten(idProvinsi) {
    $.ajax({
        type: 'post',
        data: {
            'idProvinsi': idProvinsi
        },
        url: domain + "filter/getKabupaten",
        dataType: 'json',
        success: function (data) {
            var opt;
            opt = "<option value='0'>- Select Kabupaten -</option>";
            $.each(data, function (k, v) {
                opt += "<option value=" + v.id + " >" + v.name + "</option>";
            });

            $("#kabupaten").append(opt);
        }
    });
}

function getKecamatan(idKabupaten) {
    $.ajax({
        type: 'post',
        data: {
            'idKabupaten': idKabupaten
        },
        url: domain + "filter/getKecamatan",
        dataType: 'json',
        success: function (data) {
            var opt;
            opt = "<option value='0'>- Select Kecamatan -</option>";
            $.each(data, function (k, v) {
                opt += "<option value=" + v.id + " >" + v.name + "</option>";
            });

            $("#kecamatan").append(opt);
        }
    });
}

function getDesa(idKecamatan) {
    $.ajax({
        type: 'post',
        data: {
            'idKecamatan': idKecamatan
        },
        url: domain + "filter/getDesa",
        dataType: 'json',
        success: function (data) {
            var opt;
            opt = "<option value='0'>- Select Desa -</option>";
            $.each(data, function (k, v) {
                opt += "<option value=" + v.id + " >" + v.name + "</option>";
            });

            $("#desa").append(opt);
        }
    });
}

function getTahunAjaran() {
    $.ajax({
        type: 'post',
        url: domain + "filter/getTahunAjaran",
        dataType: 'json',
        success: function (data) {
            var opt;
            opt = "<option value='0'>- Select Tahun Ajaran -</option>";
            $.each(data, function (k, v) {
                opt += "<option value=" + v.id_ta + " >" + v.tahun_awal +"/"+v.tahun_akhir+" - "+ (v.semester == 1 ? "Ganjil" : "Genap") + "</option>";
            });

            $("#tahun_ajaran").append(opt);
        }

    });
}

function CheckCode(code, field, table) {
    var jqxhr = $.ajax({
        type: 'post',
        data: {
            'code': code,
            'field': field,
            'table': table
        },
        url: domain + "filter/checkCode",
        global: false,
        async: false,
        success: function (data) {
            num = data[0].total;
        }
    }).responseText;
    return (jqxhr);
}