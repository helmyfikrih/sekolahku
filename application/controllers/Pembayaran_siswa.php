<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran_siswa extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $session_data = $this->session->userdata('logged_in');

        if (empty($session_data)) {
            redirect('login', 'refresh');
        }
        //Do your magic here
        $this->data['domain'] = base_url();
        $this->data['title'] = 'Home';
        $menu_body     = $this->menu_model->getmenu($session_data['id'], $session_data['username']);
        $this->data['menu_body'] = $menu_body;
    }


    public function index()
    {
        $this->template->load('default', 'v_home', $this->data);
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }
}

/* End of file Home.php */
