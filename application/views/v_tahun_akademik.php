<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button> -->
                    <button type="button" class="btn btn-box-tool" id="add-group" onclick="clearData()" title=" Add New Tahun Akademik">
                        <i class="fa fa-plus"></i><span> Tahun Akademik</span></button>
                    <button class="btn btn-box-tool" data-toggle="dropdown" href="#">
                        <i class="fa fa-upload"></i> UPLOAD
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="assets/template/outlet_realisasi_target_promo.xls"><i class="fa fa-download"></i> Download Template&nbsp;&nbsp;</a></li>
                        <!-- <li><a data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modal-upload" onclick="Upload()"><i class="icon-upload"></i> Upload&nbsp;&nbsp;</a></li> -->
                        <li><a onclick="Upload();"><i class="fa fa-upload"></i> Upload&nbsp;&nbsp;</a></li>
                    </ul>
                    <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button> -->
                </div>
            </div>
            <div class="box-body">

                <div class="toggle-add-group" style="display:none; background-color:#EEEEEE;width:800px;min-height:100px;position:absolute;z-index:9; padding:10px; margin-top:-10px">
                    <style>
                        .row {
                            margin-left: -15px;
                            margin-right: -15px;
                        }

                        .col-md-6 {
                            width: 45%;
                            position: relative;
                            min-height: 1px;
                            padding-left: 15px;
                            padding-right: 15px;
                            float: left;
                        }
                    </style>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <table border="0" cellspacing="10" cellpadding="10" width="100%">
                                <input type="hidden" id="txt_id" name="txt_id" />
                                <tr>
                                    <td>Tahun Awal</td>
                                    <td><input type="text" placeholder="User Name" id="username" name="username" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>Tahun Akhir</td>
                                    <td><input type="text" placeholder="password" id="password" name="password" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>Semester</td>
                                    <td><input type="text" placeholder="name" id="name" name="name" class="form-control" /></td>
                                </tr>
                                <!-- <tr>
                  <td valign="top">Region</td>
                  <td>
                    <div id="selector"><select multiple="region" name="region" id="id_region" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Distributor</td>
                  <td>
                    <div id="selector"><select multiple="subbranch" name="subbranch" id="id_subbranch" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Territory</td>
                  <td>
                    <div id="selector"><select multiple="branch" name="branch" id="id_branch" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Salesman</td>
                  <td>
                    <div id="selector"><select multiple="salesman" name="salesman" id="id_salesman" class="m-wrap scroll-select">

                      </select></div>
                  </td>
                </tr> -->
                                <tr>
                                    <td></td>
                                    <td>
                                        <button name="" id="" class="btn btn-info" onclick="input_user_data()">Submit</button>&nbsp;
                                        <button name="" id="" class="btn btn-info" onclick="remove_toggle('toggle-add-group'); clearData();">Cancel</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun Awal</th>
                                <th>Tahun Akhir</th>
                                <th>Semester</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Tahun Awal</th>
                                <th>Tahun Akhir</th>
                                <th>Semester</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="modal fade" id="modal-upload">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Upload</h4>
                        </div>
                        <div class="modal-body">
                            <!-- <p>Select File To Upload!</p> -->
                            <!-- <div id="fileuploader">Upload</div> -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="upload_file">Submit</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Upload -->
<div id="dialog_upload" title="UPLOAD">
    <div id="uploader">
    </div>
</div>
<div id="upload_confirm" class="upload_confirm" style="display:none" title="Upload this file?">
    <p><span class="icon icon-warning-sign"></span>
        Are you sure, to Upload This File?</p>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= $domain ?>assets/js/master/filter.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/js/master/tahun_akademik.js" type="text/javascript"></script>
<!-- jQuery -->
<!-- <script src="<?= $domain ?>assets/template/vendors/jquery/dist/jquery.min.js"></script> -->


<!-- END PAGE LEVEL PLUGINS -->

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
    var domain = '<?= $domain ?>';
</script>


<script type="text/javascript">
    var table;
    $(document).ready(function() {
        var buttonCommon = {
            exportOptions: {
                format: {
                    body: function(data, row, column, node) {
                        // Strip $ from salary column to make it numeric
                        return column === 5 ?
                            data.replace(/[$,]/g, '') :
                            data;
                    }
                }
            }
        };

        //datatables
        loadTable();

        //LOAD FOR INPUT/EDIT


        $('#id_region').change(function() {
            $('#id_subbranch option').remove();
            $('#id_branch option').remove();
            loadSubbranchMulti($('#id_region').val(), '');
        });

        $('#id_subbranch').change(function() {
            $('#id_branch option').remove();
            loadBranchMulti($('#id_subbranch').val(), '');
            loadSalesmanMultiOrderWeb($('#id_subbranch').val(), '');
        });

        $('#add-group').click(function() {
            clearData()
            $("#box_1").attr("checked", "checked");
            $("#password").attr("placeholder", "password");
            // loadRegionMulti('');
            $('.toggle-add-group').toggle('slow');
            $('table').css({
                'border-spacing': 'none',
                'border-collapse': 'none'
            })
            return false;
        });
    });
</script>