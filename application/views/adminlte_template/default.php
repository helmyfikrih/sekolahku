<!DOCTYPE html>
<?php
include "layout/_header.html";
include "layout/_topbar.html";
?>

<?php include "layout/_pageheader.html"; ?>

<?= $body ?>

<?php include "layout/_footer.html"; ?>
<?php include "layout/_scripts.html"; ?>