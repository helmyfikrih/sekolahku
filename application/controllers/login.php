<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->data['domain'] = base_url();
        $session_data = $this->session->userdata('logged_in');

        if (!empty($session_data)) {
            redirect('home', 'refresh');
        }
    }

    function index()
    {
        $pesan = '';
        $w = $this->input->get('w');
        if ($w == 1) {
            $pesan = "Wrong Username OR Password";
        } else {
            $pesan = "Wrong Type Code";
        }
        $data = array(
            'domain' => base_url(),
            'message' => $pesan

        );
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        $this->load->helper(array('form'));
        $this->load->view('v_login', $data);
    }
}

/* End of file Login.php */
