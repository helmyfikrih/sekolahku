<!DOCTYPE html>
<?php
include "layout/dashboard_header.html";
include "layout/_topbar.html";
?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
      <!-- BEGIN SIDEBAR -->
     
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div class="page-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
		 <?php include "layout/_pageheader.html"; ?>
          
            <?=$body?>
         </div>
         <!-- END PAGE CONTAINER-->    
      </div>
      <!-- END PAGE -->
   </div>
   <!-- END CONTAINER -->

<?php include "layout/dashboard_scripts.html"; ?>
<?php include "layout/_footer.html"; ?>
