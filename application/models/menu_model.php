<?php

Class Menu_model extends CI_Model
{
	function getmenu($id_user, $username)
	{
		$uri= $this->uri->segment(1);
		if ( empty($id_user) OR empty($username) ) {
			return FALSE;
		}	
		$sqlmenu0 		= "select allow_menu from ppdb.users where id_user = ".$id_user." and username = '".$username."'";
	
		$result0		= $this->db->query($sqlmenu0) or die('died 1');
		foreach ($result0->result_array() as $line0) {
			$allow_menu = $line0['allow_menu'];
		}
		// die($allow_menu);
		if ( strtolower($allow_menu) == 'all' ) {
			$in_menu = '';
		} else {
			$menu_no 	= str_replace(',',',',$allow_menu);
			$in_menu	= 'and id in ('.$menu_no.')';
		}
		
		$menu_no 	= str_replace(',','","',$allow_menu);
		$body		= '';
		$sqlmenu 	= 'select id, url, name, parent, child, icon from ppdb.menu where status = 1 and parent = 0 '.$in_menu.' order by sort asc';
		// die($sqlmenu);
		$result		= $this->db->query($sqlmenu) or die('died 2');
		// echo "getMenu"; die;
		// echo json_encode($result); die;
		foreach ($result->result_array() as $line) {
			// die($line['child']);
			if ( $line['child'] == 0 ) {
				$classActive='';
				if($uri== $line['url']){
					$classActive = 'active';
				}
				$body .= '<li class="'.$classActive	.'"><a href="'.base_url().$line['url'].'"><i class="'. $line['icon']. '"></i><span>' . ucwords($line['name']) . '</span></a></li>';
				$classActive = '';
			} else {
				$sqlGetParentFromUri2 = "select * from menu where url like '%".$uri."%'";
				$prentId2 = $this->db->query($sqlGetParentFromUri2)->row();
				$sqlGetParentFromUri = "select * from menu where id = ".$prentId2->parent;
				$prentId = $this->db->query($sqlGetParentFromUri)->row();
				// print_r($sqlGetParentFromUri);
				// print_r($line['id'].'_');
				if(isset($prentId))
				{
					if ($prentId->parent == $line['id']) {
						// print_r($prentId->parent. '_');
						$classActive = 'active';
					}
					if ($prentId->id == $line['id']) {
						// print_r($prentId->parent. '_');
						$classActive = 'active';
					}
				}
				
				$body .= '<li class="treeview ' . $classActive.'">
						<a href="#"><i class="' . $line['icon']. '"></i> <span>' . ucwords($line['name']) . '</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">';
				$classActive = '';
				$sqlmenu2 	= 'select id, url, name, parent, child, icon from menu where status = 1 and parent = '.$line['id'].' '.$in_menu.' order by sort asc';
				$result2	= $this->db->query($sqlmenu2) or die('died 3');
				foreach ($result2->result_array() as $line2) {
					
					if ( $line2['child'] == 0 ) {
						if ($uri == $line2['url']) {
							$classActive = 'active';
						}
						$body .= '<li class="'.$classActive.'"><a href="'.base_url().$line2['url']. '"><i class="fa fa-circle-o"></i><span>'.$line2['name']. '</span></a></li>';
						$classActive = '';
					} else {
						// print_r($prentId2->id);
						if(isset($prentId))
						{
							if ($prentId->id == $line2['id']) {
								$classActive = 'active';
							}
						}
						
						$body .= '<li class="treeview '.$classActive.'">
						<a href="#"><i class="' . $line2['icon'] . '"></i> <span>' . $line2['name'] . '</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">';
						$classActive = '';
						// $body .= '<li>
						// <a><i class="' . $line2['icon'] . '"></i>' . $line2['name'] . '<span class="fa fa-chevron-down"></a>
						// <ul class="nav child_menu">';
								
						$sqlmenu3 	= 'select id, url, name, parent, child, icon from ppdb.menu where status = 1 and  parent = '.$line2['id'].' '.$in_menu.' order by sort asc';
						// die($sqlmenu3);
						$result3	= $this->db->query($sqlmenu3) or die('died 4');
						foreach ($result3->result_array() as $line3) {
						
							if ( $line3['child'] == 0 ) {
								if ($uri == $line3['url']) {
									$classActive = 'active';
								}
								$body .= '<li class="'. $classActive.'"><a href="'.base_url().$line3['url']. '"><i class="fa fa-circle-o"></i>'.$line3['name'].'</a></li>';
								$classActive = '';
							} else {
							

									$body .= '<li class="dropdown-submenu">
												<a class="dropdown-toggle" href="#">'.$line3['name'].'<span class="arrow"></span></a>
												<ul class="dropdown-menu">';
								
									$sqlmenu4 	= 'select id, url, name, parent, child from menu where status = 1 and  parent = '.$line3['id'].' '.$in_menu.' order by sort asc';
									$result4	= $this->db->query($sqlmenu4) or die('died 5');
									foreach ($result4->result_array() as $line4) {
										if ( $line4['child'] == 0 ) {
											$body .= '<li><a href="'.base_url().$line4['url'].'">'.$line4['name'].'</a></li>';
										} else {
							
									
											$body .= '<li class="dropdown-submenu">
														<a class="dropdown-toggle" href="#">'.$line4['name'].'<span class="arrow"></span></a>
														<ul class="dropdown-menu">';
										
											$sqlmenu5 	= 'select id, url, name, parent, child from menu where status = 1 and  parent = '.$line4['id'].' '.$in_menu.' order by sort asc';
											$result5	= $this->db->query($sqlmenu5) or die('died 6');	
											foreach ($result5->result_array() as $line5) {
												$body .= '<li><a href="'.base_url().$line5['url'].'">'.$line5['name'].'</a></li>';
											}
											
											$body .= '
												</ul>
											</li>';
										}	
									}
									
									$body .= '
												</ul>
											</li>';
							
							}
						
							
						}
						
						$body .= '
								</ul>
								</li>';
						
					}
					
				}	
				
				
				$body .= '                
                        </ul>
                        <b class="caret-out"></b>                        
						</li>';
						
				
				
			}
			
			
		}
		// die;
		return $body;
		
	}
	
	function UserAllow($id_user){
		$query ="SELECT allow_menu FROM users WHERE id_user = $id_user LIMIT 1";
        $q = $this->db->query($query);
        return $q->result_array();
	}
	
	function idMenu($urlmenu){
		$query ="SELECT id, access FROM menu WHERE status = 1 and url = '$urlmenu' ORDER BY id ASC LIMIT 1";
        $q = $this->db->query($query);
        return $q->result_array();
	}
	
	function routing($id){
		$query ="SELECT id,url,name,access FROM menu WHERE status = 1 and parent = $id AND id<>$id  ORDER BY sort";
        $q = $this->db->query($query);
        return $q->result();
	}
	
	function getmenujson(){
		
		$menu = array();
		$menu_top = $this->routing(0);
		$menu = $menu_top;
		
		$n1=0;
		foreach($menu as $top){
			$menu['root'][$n1]['id'] = $top->id;
			$menu['root'][$n1]['url'] = $top->url;
			$menu['root'][$n1]['name'] = $top->name;
			
			$col = $this->routing($top->id);
			if(count($col)<>0){
				$menu['sub_'.$top->id] = $col;
			}
			
			$n2=0;
			foreach($col as $top2){
				$col2 = $this->routing($top2->id);
				if(count($col2)<>0){
					$menu['sub_'.$top->id.'_'.$top2->id] = $col2;
				}
					$n3=0;
					foreach($col2 as $top3){
						$col3 = $this->routing($top3->id);
						if(count($col3)<>0){
							$menu['sub_'.$top->id.'_'.$top2->id.'_'.$top3->id] = $col3;
						}
						
					$n3++;	
					}
				$n2++;
				
			}

			$n1++;
		}
		
		return $menu;
	
	}
	
	function _load_region(){
		$query ="SELECT * from data_region";
        $q = $this->db->query($query);
		return $q->result_array();
	}
	
	function _load_branch_old($id_region){
		$query ="SELECT * from data_branch where id_region in($id_region)";
        $q = $this->db->query($query);
		return $q->result_array();
	}
	
	function _load_branch($id_subbranch) {
		$query ="SELECT a.id_branch, b.code_branch, b.name_branch FROM  data_territory_distributor a 
				join data_branch b ON a.id_branch = b.id_branch  
				where a.id_subbranch IN ($id_subbranch)";
	
        $q = $this->db->query($query);
        return $q->result_array();
	}
	
	function _load_subbranch_old($id_branch){
		$query ="SELECT * from data_subbranch where id_branch in($id_branch)";
        $q = $this->db->query($query);
		return $q->result_array();
	}	
	
	function _load_subbranch_old2($id_branch){
		$query ="SELECT b.* FROM  data_territory_distributor a 
				join data_subbranch b ON a.id_subbranch = b.id_subbranch  
				where a.id_branch in($id_branch)";
	
        $q = $this->db->query($query);
        return $q->result_array();
	}
	
	function _load_subbranch($id_region){
		$query ="SELECT * from data_subbranch where id_region in($id_region)";
        $q = $this->db->query($query);
		return $q->result_array();
	}
	
	function _load_salesman($id_subbranch){
		$query ="SELECT * from data_salesman where id_subbranch in($id_subbranch)";
	
        $q = $this->db->query($query);
		
		return $q->result_array();
	}
	
}
?>