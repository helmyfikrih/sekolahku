<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register_siswa extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $session_data = $this->session->userdata('logged_in');

        if (empty($session_data)) {
            redirect('login', 'refresh');
        }
        //Do your magic here
        $this->data['domain'] = base_url();
        $this->data['title'] = 'Registrasi Siswa Baru';
        $this->data['page_title'] = 'PPDB - Registrasi Siswa Baru';
        $this->data['desc'] = 'Form Registrasi Siswa Baru';

        $menu_body     = $this->menu_model->getmenu($session_data['id'], $session_data['username']);
        $this->data['menu_body'] = $menu_body;
    }


    public function index()
    {
        $this->template->load('default', 'v_register_siswa', $this->data);
    }
}

/* End of file Home.php */
