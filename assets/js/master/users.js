function Upload() {
    $("#uploader").html('');
    $("#uploader").html('<div id="fileuploader">Select File</div>');

    var fileStatus = false;
    var extraObj = $("#fileuploader").uploadFile({

        url: domain + "users/uploadUsers",
        fileName: "namafile",
        method: "POST",
        multiple: false,
        allowedTypes: "xls,xlsx",
        maxFileCount: 1,
        autoSubmit: false,
        returnType: "json",
        onSelect: function (files) {
            fileStatus = true;
        },
        onSubmit: function (files) {
            $("#dialog_upload").dialog("close");
            $("#loading").show();
        },
        afterUploadAll: function (obj) {

            $("#loading").hide();

            var title = obj.responses[0].title;
            var icon = obj.responses[0].icon;
            var message = obj.responses[0].messages;
            var flag_message = obj.responses[0].message;
            const wrapper = document.createElement('div');
            wrapper.innerHTML = message;
            swal({
                title: title,
                icon: icon,
                // customClass: 'swal-wide',
                closeOnClickOutside: false,
                closeOnEsc: false,
                content: wrapper
            });
            clearData();
            loadTable('', '');
            // location.reload(true);
        }
    });

    $("#dialog_upload").dialog({
        dialogClass: 'ui-dialog-red',
        autoOpen: false,
        resizable: false,
        height: 300,
        width: 550,
        modal: true,
        show: {
            effect: "slide",
            duration: 500
        },
        hide: {
            effect: "clip",
            duration: 500
        },
        buttons: [
            {
                "text": "Upload",
                'class': 'btn blue',
                click: function () {
                    if (fileStatus) {
                        $(".upload_confirm").dialog({
                            dialogClass: 'ui-dialog-green',
                            height: 210,
                            modal: true,
                            buttons: [
                                {
                                    'class': 'btn green',
                                    "text": "OK",
                                    click: function () {
                                        $("#upload_confirm").dialog("close");
                                        extraObj.startUpload();

                                    }
                                },
                                {
                                    'class': 'btn',
                                    "text": "Cancel",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        });
                    } else {
                        alert("Pilih file terlebih dahulu!");
                    }

                }
            },
            {
                "text": "Close",
                'class': 'btn red',
                click: function () {
                    // location.reload(true);
                    loadTable('', '');

                    $(this).dialog("close");

                }
            }
        ]
    })
    // .prev(".ui-dialog-titlebar").css("background", "blue")
    ;

    $("#dialog_upload").dialog("open");

}

function loadTable() {
    $("#datatable-responsive").dataTable().fnDestroy();
    return $('#datatable-responsive').DataTable({
        "scrollX": true,
        "processing": false,
        "serverSide": true,
        "order": [],

        "ajax": {
            "url": "users/get_data_user",
            "type": "POST",
            // "data": {
            //     "user_id": 451,
            //     "rayon": "asas",
            //     "test": "test",
            // }
        },

        "columnDefs": [{
            "targets": [0, 3],
            "orderable": false,
        },],

    });
}

function getID(e) {
    var t = e.id;
    alert(e.id)
}

function loading(e) {
    var t = jQuery("." + e).parents(".portlet");
    App.blockUI(t);
    window.setTimeout(function () {
        App.unblockUI(t)
    }, 1e3)
}

function remove_toggle(e) {
    jQuery("." + e).toggle("slow")
}

function close_dialog(e) {
    $("#" + e).dialog("close")
}

function input_user_data() {
    if (jQuery('#username').val() === '' ||
        jQuery('#name').val() === ''
    ) {
        alert("Fill the values");
        return;
    }

    if (jQuery('#txt_id').val() == '') {
        var check = parseInt(CheckCode(jQuery('#username').val(), 'username', 'users'));
        if (check > 0) {
            alert("Username is already");
            return;
        }
    }


    $('button[type=submit]').prop('disabled', true);

    ngeklik();
    // loading("tab-content");
    var e = jQuery("#txt_id").val();
    var t = jQuery("#username").val();
    var n = jQuery("#password").val();
    var r = jQuery("#email").val();
    var i = jQuery("#name").val();
    // var s = jQuery("#iregion").html();
    // var o = jQuery("#ibranch").html();
    // var u = jQuery("#isubbranch").html();
    // var a = jQuery("#isalesman").html();
    //	var role = jQuery("#role").val();
    var f = jQuery("#imenu").html();

    // var region = [];
    // $('#id_region :selected').each(function (i, selectedElement) {
    //     region[i] = $(selectedElement).val();

    // });
    // var subbranch = [];
    // $('#id_subbranch :selected').each(function (i, selectedElement) {
    //     subbranch[i] = $(selectedElement).val();

    // });
    // var branch = [];
    // $('#id_branch :selected').each(function (i, selectedElement) {
    //     branch[i] = $(selectedElement).val();

    // });
    // var salesman = [];
    // $('#id_salesman :selected').each(function (i, selectedElement) {
    //     salesman[i] = $(selectedElement).val();

    // });

    // console.log(region);
    jQuery.ajax({
        url: domain + "users/save_user",
        dataType: "json",
        type: "POST",
        beforeSend: function () { $('#loading').show(); },
        complete: function () { $('#loading').hide(); },
        data: {
            id: e,
            username: t,
            password: n,
            name: i,
            email: r,
            //			role : role,
            level: f
        },
        success: function (e) {
            $('button[type=submit]').prop('disabled', false);
            if (e) {
                // load_table_sales_cost("");
                clearData();
                remove_toggle("toggle-add-group");
                swal("Good job!", e.message, "success");
            } else {
                // alert(e)
                swal("Good job!", e.message, "success");
            }
            loadTable();
        }
    })
}

function deleteData(e) {
    $(".dialog_confirm").dialog({
        dialogClass: "ui-dialog-green",
        height: 210,
        modal: true,
        buttons: [{
            "class": "btn red",
            text: "Delete",
            click: function () {
                execDelete(e)
            }
        }, {
            "class": "btn",
            text: "Cancel",
            click: function () {
                $(this).dialog("close")
            }
        }
        ]
    })
}

function clearData() {
    jQuery("#txt_id").val("");
    jQuery("#username").val("");
    jQuery("#password").val("");
    jQuery("#name").val("");
    jQuery("#email").val("");
    jQuery("#id_region").html("");
    jQuery("#iregion").html("");
    jQuery("#id_branch").html("");
    jQuery("#ibranch").html("");
    jQuery("#id_subbranch").html("");
    jQuery("#isubbranch").html("");
    jQuery("#id_salesman").html("");
    jQuery("#isalesman").html("");
    //	jQuery("#role").val("");
    // jQuery("#imenu").html("");
    $('input:checkbox').removeAttr('checked');
    $("#accordion3 input:checkbox").prop("checked", false);
}

function execDelete(e) {
    $.ajax({
        type: "post",
        data: {
            id: e
        },
        dataType: "json",
        url: domain + "user/delete",
        success: function (e) {
            $(".dialog_confirm").dialog("close");
            clearData();
            load_table_sales_cost("", "")
        }
    })
}

function editData(e) {
    jQuery.ajax({
        type: "post",
        data: {
            id: e
        },
        url: domain + "users/getOne",
        dataType: "json",
        success: function (e) {
            
            $("#password").attr("placeholder", "leave blank if don't change");
            jQuery("#txt_id").val(e[0].id_user);
            jQuery("#username").val(e[0].username);
            //jQuery("#password").val(e[0].password);
            jQuery("#name").val(e[0].name);
            jQuery("#email").val(e[0].email);
            //			jQuery("#role").val(e[0].role);
            $('input:checkbox').prop("checked", false);
            var t = e[0].allow_menu.split(",");
            for (i = 0; i < t.length; i++) {
                $("#box_" + t[i]).prop("checked", true);
            }
            // $('#id_region option').remove();
            // $('#id_subbranch option').remove();
            // $('#id_branch option').remove();
            // $('#id_salesman option').remove();



            //=========dropdown===============
            // var arrRegion = [];
            // $.each(e.Region, function (k, v) {
            //     arrRegion[k] = v.id_region;

            // });
            // $.ajax({
            //     type: 'post',
            //     url: domain + "filter/getRegion",
            //     dataType: 'json',
            //     success: function (data) {
            //         var opt;
            //         var cek;

            //         $.each(data, function (k, v) {
            //             cek = $.inArray(v.id_region, arrRegion);

            //             if (cek == '-1') { var slc = ""; } else { var slc = "selected"; }
            //             opt += "<option value=" + v.id_region + " " + slc + " >" + v.code_region + ' - ' + v.name_region + "</option>";

            //         });

            //         $("#id_region").append(opt);
            //         //subbranch start
            //         var region = [];
            //         $('#id_region :selected').each(function (i, selectedElement) {
            //             region[i] = $(selectedElement).val();
            //         });
            //         var arrSubbranch = [];
            //         $.each(e.subbranch, function (k, v) {
            //             arrSubbranch[k] = v.id_subbranch;

            //         });

            //         $.ajax({
            //             type: 'post',
            //             data: {
            //                 'id_region': arrRegion
            //             },
            //             url: domain + "filter/getSubbranchMulti",
            //             dataType: 'json',
            //             success: function (data) {
            //                 var opt;


            //                 $('#id_subbranch option').remove();
            //                 $.each(data, function (k, v) {
            //                     var cek = $.inArray(v.id_subbranch, arrSubbranch);
            //                     if (cek == '-1') { var slc = ""; } else { var slc = "selected"; }
            //                     opt += "<option value=" + v.id_subbranch + " " + slc + " >" + v.code_subbranch + ' - ' + v.name_subbranch + "</option>";
            //                 });

            //                 $("#id_subbranch").append(opt);
            //                 //branch start
            //                 var subbranch = [];
            //                 $('#id_subbranch :selected').each(function (i, selectedElement) {
            //                     subbranch[i] = $(selectedElement).val();

            //                 });
            //                 var arrBranch = [];
            //                 $.each(e.branch, function (k, v) {
            //                     arrBranch[k] = v.id_branch;
            //                 });
            //                 $.ajax({
            //                     type: 'post',
            //                     data: {
            //                         'id_region': arrRegion,
            //                         'id_subbranch': arrSubbranch
            //                     },
            //                     url: domain + "filter/getBranchMulti",
            //                     dataType: 'json',
            //                     success: function (data) {
            //                         var opt;

            //                         $('#id_branch option').remove();
            //                         $.each(data, function (k, v) {
            //                             var cek = $.inArray(v.id_branch, arrBranch);
            //                             if (cek == '-1') { var slc = ""; } else { var slc = "selected"; }
            //                             opt += "<option value=" + v.id_branch + " " + slc + " >" + v.code_branch + ' - ' + v.name_branch + "</option>";
            //                         });

            //                         $("#id_branch").append(opt);
            //                         //salesman start
            //                         var branch = [];
            //                         $('#id_branch :selected').each(function (i, selectedElement) {
            //                             branch[i] = $(selectedElement).val();

            //                         });

            //                         var arrSalesman = [];
            //                         $.each(e.salesman, function (k, v) {
            //                             arrSalesman[k] = v.id_salesman;

            //                         });
            //                         $.ajax({
            //                             type: 'post',
            //                             data: {
            //                                 'id_region': arrRegion,
            //                                 'id_branch': arrBranch,
            //                                 'id_subbranch': arrSubbranch
            //                             },
            //                             url: domain + "filter/getSalesmanMulti",
            //                             dataType: 'json',
            //                             success: function (data) {
            //                                 var opt;

            //                                 $('#id_salesman option').remove();
            //                                 $.each(data, function (k, v) {
            //                                     var cek = $.inArray(v.id_salesman, arrSalesman);
            //                                     if (cek == '-1') { var slc = ""; } else { var slc = "selected"; }
            //                                     // console.log(cek);
            //                                     opt += "<option value=" + v.id_salesman + " " + slc + " >" + v.code_salesman + ' - ' + v.name_salesman + "</option>";
            //                                 });

            //                                 $("#id_salesman").append(opt);
            //                             }

            //                         });
            //                     }

            //                 });

            //             }
            //         });
            //     }

            // });





            // loadSubbranchMulti( region, arrSubbranch);

            // var subbranch = [];
            // $('#id_subbranch :selected').each(function(i, selectedElement) {
            // subbranch[i] = $(selectedElement).val();

            // });		

            // var arrBranch =[];
            // $.each(e.branch, function(k,v){
            // arrBranch[k] = v.id_branch;

            // });

            // loadBranchMulti( region, subbranch, arrBranch);	


            // loadSalesmanMulti( region, branch, subbranch, arrSalesman);	



        }
    });
    jQuery(".toggle-add-group").show("slow")
}

function ngeklik() {
    var e = [];
    $("#accordion3 input:checked").each(function () {
        e.push($(this).val())
    });
    $("#imenu").html(e.toString())
}

function load_table_sales_cost(e, t) {
    if (e != "") {
        var e = e
    } else {
        var e = domain + "user/load_table"
    }
    var n = '<table class="table table-striped table-bordered" id="group-list">';
    n += "<thead>";
    n += "	<tr>";
    n += '	<th><center>ID</center></th>';
    n += '	<th><center>User Name</center></th>';
    n += '	<th><center>Full Name</center></th>';
    n += '	<th><center>Email</center></th>';
    n += '	<th><center>Last Login</center></th>';
    n += '	<th><center>IP</center></th>';
    n += '	<th><center>Action</center></th>';
    n += "</tr>";
    n += "</thead>";
    n += '<tbody id="data-table">';
    jQuery.ajax({
        url: e,
        dataType: "json",
        type: "POST",
        data: {
            txt_src: $("#txt_src").val()
        },
        success: function (e) {
            jQuery("#table-append-group-list").find("#group-list").remove();
            var t = "";
            //$.each(e.result, function (e, n) {
            //	t += '<tr class="odd gradeX"><td>' + n.id_user + "</td><td>" + n.username + "</td><td>" + n.name + "</td><td>" + n.email + '</td><td><center><a href="javascript:;" onClick="editData(\'' + n.id_user + '\')" style="padding:0px 8px;" class="btn green">Edit</a> <a href="javascript:;" style="padding:0px 8px;" class="btn red icn-only" onClick="deleteData(\'' + n.id_user + "')\">Delete</a></center></td></tr>"
            //});
            t = e.result;
            jQuery("#txt_specific_date").val(e.result.custom_date);
            $("#paging").html(e.pagination);
            $("#total").html("Total: " + e.total[0].total);
            $("#table-append-group-list").append(n + t + "</tbody></table>")
        }
    })
}

function handleTablesGroupList() {
    if (!jQuery().dataTable) {
        return
    }
    $("#group-detail").dataTable({
        aLengthMenu: [[10, 25, 50, -1], [5, 15, 20, "All"]],
        iDisplayLength: 5,
        sDom: "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        sPaginationType: "bootstrap",
        oLanguage: {
            sLengthMenu: "_MENU_ records per page",
            oPaginate: {
                sPrevious: "Prev",
                sNext: "Next"
            }
        },
        aoColumnDefs: [{
            bSortable: false,
            aTargets: [0]
        }
        ]
    });
    jQuery("#group-detail .group-checkable").change(function () {
        var e = jQuery(this).attr("data-set");
        var t = jQuery(this).is(":checked");
        jQuery(e).each(function () {
            if (t) {
                $(this).attr("checked", true)
            } else {
                $(this).attr("checked", false)
            }
        });
        jQuery.uniform.update(e)
    });
    jQuery("#group-detail_wrapper .dataTables_filter input").addClass("m-wrap medium");
    jQuery("#group-detail_wrapper .dataTables_length select").addClass("m-wrap xsmall")
}
/*
$("#region").change(function () {
	$('#branch option').remove();
	$('#subbranch option').remove();
	$('#salesman option').remove();
	
	jQuery("#ibranch").html("");
	jQuery("#isubbranch").html("");
	jQuery("#isalesman").html("");
	
	var e = [];
	$("#region :selected").each(function (t, n) {
		e[t] = $(n).val()
	});
	$("#iregion").html(e.toString());
	jQuery.ajax({
		url : domain + loadx2,
		dataType : "json",
		type : "POST",
		data : {
			id_region : e.toString()
		},
		success : function (e) {
			if (e.length != 0) {
				var t = "";
				for (var n = 0; n < e.length; n++) {
					t += '<option id="subbranch_' + e[n].id_subbranch + '" value="' + e[n].id_subbranch + '">' + e[n].name_subbranch + "</option> "
				}
				$("#subbranch").html(t)
			} else {
				$("#subbranch").html("")
			}
		}
	})
});
*/
/*
$("#branch").change(function () {
	var e = [];
	$("#branch :selected").each(function (t, n) {
		e[t] = $(n).val()
	});
	$("#ibranch").html(e.toString());
	jQuery.ajax({
		url : domain + loadx2,
		dataType : "json",
		type : "POST",
		data : {
			id_branch : e.toString()
		},
		success : function (e) {
			if (e.length != 0) {
				var t = "";
				for (var n = 0; n < e.length; n++) {
					t += '<option id="subbranch_' + e[n].id_subbranch + '" value="' + e[n].id_subbranch + '">' + e[n].name_subbranch + "</option> "
				}
				$("#subbranch").html(t)
			} else {
				$("#subbranch").html("")
			}
		}
	})
});
*/

$("#subbranch").change(function () {

    $('#branch option').remove();
    $('#salesman option').remove();

    jQuery("#ibranch").html("");
    jQuery("#isalesman").html("");

    var e = [];
    $("#subbranch :selected").each(function (t, n) {
        e[t] = $(n).val()
    });
    $("#isubbranch").html(e.toString());

    jQuery.ajax({
        url: domain + "user/load_salesman",
        dataType: "json",
        type: "POST",
        data: {
            id_subbranch: e.toString()
        },
        success: function (e) {
            if (e.length != 0) {
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    t += '<option id="salesman_' + e[n].id_salesman + '" value="' + e[n].id_salesman + '">' + e[n].name_salesman + "</option> "
                }
                $("#salesman").html(t)
            } else {
                $("#salesman").html("")
            }
        }
    })


    var z = [];

    jQuery.ajax({
        url: domain + loadx1,
        dataType: "json",
        type: "POST",
        data: {
            id_subbranch: e.toString()
        },
        success: function (z) {
            if (z.length != 0) {
                var t = "";
                for (var w = 0; w < z.length; w++) {
                    t += '<option id="branch_' + z[w].id_branch + '" value="' + z[w].id_branch + '">' + z[w].name_branch + "</option> "
                }
                $("#branch").html(t)
            } else {
                $("#branch").html("")
            }
        }
    })


});



$("#salesman").change(function () {
    var e = [];
    $("#salesman :selected").each(function (t, n) {
        e[t] = $(n).val()
    });
    $("#isalesman").html(e.toString())
});

$("#branch").change(function () {
    var e = [];
    $("#branch :selected").each(function (t, n) {
        e[t] = $(n).val()
    });
    $("#ibranch").html(e.toString())
});

$("#accordion3 input:checkbox").click(function () {

    var e = $(this).attr("id");
    var t = $(this).val();
    jQuery.ajax({
        type: "post",
        data: {
            id: t
        },
        url: domain + "users/cekpohon",
        dataType: "json",
        success: function (e) {
            for (x = 0; x < e.length; x++) {
                $("#box_" + e[x].id).attr("checked", "checked")
            }
        }
    });
    if ($(this).is(":checked")) {
        $(".down_" + t + " :checkbox").prop("checked", true)
    } else {
        $(".down_" + t + " :checkbox").prop("checked", false)
    }
})
