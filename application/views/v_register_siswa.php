<style>
    .makeRed {
        border: 2px solid red;
    }

    .actionBar {
        width: 100%;
        border-top: 1px solid #ddd;
        padding: 10px 5px;
        text-align: right;
        margin-top: 10px
    }

    .actionBar .buttonDisabled {
        cursor: not-allowed;
        pointer-events: none;
        opacity: .65;
        filter: alpha(opacity=65);
        box-shadow: none
    }

    .actionBar a {
        margin: 0 3px
    }

    .form_wizard .stepContainer {
        display: block;
        position: relative;
        margin: 0;
        padding: 0;
        border: 0 solid #CCC;
        overflow-x: hidden
    }

    .wizard_horizontal ul.wizard_steps {
        display: table;
        list-style: none;
        position: relative;
        width: 100%;
        margin: 0 0 20px
    }

    .wizard_horizontal ul.wizard_steps li {
        display: table-cell;
        text-align: center
    }

    .wizard_horizontal ul.wizard_steps li a,
    .wizard_horizontal ul.wizard_steps li:hover {
        display: block;
        position: relative;
        -moz-opacity: 1;
        filter: alpha(opacity=100);
        opacity: 1;
        color: #666
    }

    .wizard_horizontal ul.wizard_steps li a:before {
        content: "";
        position: absolute;
        height: 4px;
        background: #ccc;
        top: 20px;
        width: 100%;
        z-index: 4;
        left: 0
    }

    .wizard_horizontal ul.wizard_steps li a.disabled .step_no {
        background: #ccc
    }

    .wizard_horizontal ul.wizard_steps li a .step_no {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 100px;
        display: block;
        margin: 0 auto 5px;
        font-size: 16px;
        text-align: center;
        position: relative;
        z-index: 5
    }

    .wizard_horizontal ul.wizard_steps li a.selected:before,
    .step_no {
        background: #34495E;
        color: #fff
    }

    .wizard_horizontal ul.wizard_steps li a.done:before,
    .wizard_horizontal ul.wizard_steps li a.done .step_no {
        background: #1ABB9C;
        color: #fff
    }

    .wizard_horizontal ul.wizard_steps li:first-child a:before {
        left: 50%
    }

    .wizard_horizontal ul.wizard_steps li:last-child a:before {
        right: 50%;
        width: 50%;
        left: auto
    }

    .wizard_verticle .stepContainer {
        width: 80%;
        float: left;
        padding: 0 10px
    }

    .wizard_verticle .wizard_content {
        width: 80%;
        float: left;
        padding-left: 20px
    }

    .wizard_verticle ul.wizard_steps {
        display: table;
        list-style: none;
        position: relative;
        width: 20%;
        float: left;
        margin: 0 0 20px
    }

    .wizard_verticle ul.wizard_steps li {
        display: list-item;
        text-align: center
    }

    .wizard_verticle ul.wizard_steps li a {
        height: 80px
    }

    .wizard_verticle ul.wizard_steps li a:first-child {
        margin-top: 20px
    }

    .wizard_verticle ul.wizard_steps li a,
    .wizard_verticle ul.wizard_steps li:hover {
        display: block;
        position: relative;
        -moz-opacity: 1;
        filter: alpha(opacity=100);
        opacity: 1;
        color: #666
    }

    .wizard_verticle ul.wizard_steps li a:before {
        content: "";
        position: absolute;
        height: 100%;
        background: #ccc;
        top: 20px;
        width: 4px;
        z-index: 4;
        left: 49%
    }

    .wizard_verticle ul.wizard_steps li a.disabled .step_no {
        background: #ccc
    }

    .wizard_verticle ul.wizard_steps li a .step_no {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 100px;
        display: block;
        margin: 0 auto 5px;
        font-size: 16px;
        text-align: center;
        position: relative;
        z-index: 5
    }

    .wizard_verticle ul.wizard_steps li a.selected:before,
    .step_no {
        background: #34495E;
        color: #fff
    }

    .wizard_verticle ul.wizard_steps li a.done:before,
    .wizard_verticle ul.wizard_steps li a.done .step_no {
        background: #1ABB9C;
        color: #fff
    }

    .wizard_verticle ul.wizard_steps li:first-child a:before {
        left: 49%
    }

    .wizard_verticle ul.wizard_steps li:last-child a:before {
        left: 49%;
        left: auto;
        width: 0
    }

    .form_wizard .loader {
        display: none
    }

    .form_wizard .msgBox {
        display: none
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!-- Smart Wizard -->
                <div id="smartwizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li>
                            <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                    Step 1<br />
                                    <small>Data Siswa</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">
                                    Step 2<br />
                                    <small>Alamat Siswa</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                    Step 3<br />
                                    <small>Data Orangtua atau Wali</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-4">
                                <span class="step_no">4</span>
                                <span class="step_descr">
                                    Step 4<br />
                                    <small>Persetujuan Pernyataan</small>
                                </span>
                            </a>
                        </li>

                    </ul>
                    <div id="step-1">
                        <form id="appForm" class="form-horizontal form-label-left">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12">Provinsi</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <select id="provinsi" class="form-control select2">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12">kabupaten</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <select id="kabupaten" class="form-control select2">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12">Kecamatan</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <select id="kecamatan" class="form-control select2">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12">Desa</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <select id="desa" class="form-control select2">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12" for="nama_belakang">Jalan <span class="required">*</span>
                                        </label>
                                        <div class="col-md-5 col-sm-12 col-xs-12">
                                            <input type="text" id="nama_belakang" name="nama_belakang" required="required" class="form-control">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-12 col-xs-12" for="nama_belakang">RT <span class="required">*</span>
                                        </label>
                                        <div class="col-md-1 col-sm-12 col-xs-12">
                                            <input type="text" id="nama_belakang" name="nama_belakang" required="required" class="form-control">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-12 col-xs-12" for="nama_belakang">RW <span class="required">*</span>
                                        </label>
                                        <div class="col-md-1 col-sm-12 col-xs-12">
                                            <input type="text" id="nama_belakang" name="nama_belakang" required="required" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div id="jenis_kelamin" class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="jenis_kelamin" required="required" value="male"> &nbsp; Male &nbsp;
                                                </label>
                                                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="jenis_kelamin" required="required" value="female"> Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nisn">NISN <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="nisn" name="nisn" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-12 col-xs-12">Tahun Ajaran</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="tahun_ajaran" class="form-control select2">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div id="step-2" style="display:none;">
                        <div class="row form-horizontal form-label-left">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_depan">Nama Depan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="nama_depan" name="nama_depan" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_belakang">Nama Belakang <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="nama_belakang" name="nama_belakang" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="jenis_kelamin" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="jenis_kelamin" required="required" value="male"> &nbsp; Male &nbsp;
                                            </label>
                                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="jenis_kelamin" required="required" value="female"> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nisn">NISN <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="nisn" name="nisn" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class='input-group date' id='datepicker'>
                                            <input type='text' required="required" class="form-control" readonly="readonly" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div id="step-3" style="display:none;">
                        <h2 class="StepTitle">Step 3 Content</h2>
                        <p>
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                    <div id="step-4" style="display:none;">
                        <h2 class="StepTitle">Step 4 Content</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>


                </div>
                <!-- End SmartWizard Content -->
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer">
                Footer
            </div> -->
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- Custom Theme Scripts -->
<script src="<?= $domain ?>assets/js/master/filter.js" type="text/javascript"></script>
<script src="<?= $domain ?>assets/js/master/register_siswa.js" type="text/javascript"></script>
<!-- jQuery -->
<!-- <script src="<?= $domain ?>assets/template/vendors/jquery/dist/jquery.min.js"></script> -->


<!-- END PAGE LEVEL PLUGINS -->

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
    var domain = '<?= $domain ?>';
</script>

<script type="text/javascript">
    $(document).ready(function() {

        getProvinsi();
        getTahunAjaran();
        $('#provinsi').select2({});
        $('#kabupaten').select2();
        $('#kecamatan').select2();
        $('#tahun_ajaran').select2();
        $('#kabupaten').prop('disabled', true);
        $('#kecamatan').prop('disabled', true);
        $('#desa').prop('disabled', true);
        $('#provinsi').change(function() {
            $('#kabupaten option').remove();
            $('#kecamatan option').remove();
            $('#desa option').remove();
            getKabupaten($('#provinsi').val());
            $('#kabupaten').prop('disabled', false);
        });
        $('#kabupaten').change(function() {
            $('#kecamatan option').remove();
            $('#desa option').remove();
            getKecamatan($('#kabupaten').val());
            $('#kecamatan').prop('disabled', false);
        });
        $('#kecamatan').change(function() {
            $('#desa option').remove();
            getDesa($('#kecamatan').val());
            $('#desa').prop('disabled', false);
        });
        // Smart Wizard
        // $('#smartwizard').smartWizard({
        //     selected: 0, // Initial selected step, 0 = first step 
        //     keyNavigation: true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
        //     autoAdjustHeight: true, // Automatically adjust content height
        //     cycleSteps: false, // Allows to cycle the navigation of steps
        //     backButtonSupport: true, // Enable the back button support
        //     useURLhash: true, // Enable selection of the step based on url hash
        //     lang: { // Language variables
        //         next: 'Next',
        //         previous: 'Previous'
        //     },
        //     toolbarSettings: {
        //         toolbarPosition: 'bottom', // none, top, bottom, both
        //         toolbarButtonPosition: 'right', // left, right
        //         showNextButton: true, // show/hide a Next button
        //         showPreviousButton: true, // show/hide a Previous button
        //         toolbarExtraButtons: [
        //             $('<button></button>').text('Finish')
        //             .addClass('btn btn-info')
        //             .on('click', function() {
        //                 alert('Finsih button click');
        //             }),
        //             $('<button></button>').text('Cancel')
        //             .addClass('btn btn-danger')
        //             .on('click', function() {
        //                 alert('Cancel button click');
        //             })
        //         ]
        //     },
        //     anchorSettings: {
        //         anchorClickable: true, // Enable/Disable anchor navigation
        //         enableAllAnchors: false, // Activates all anchors clickable all times
        //         markDoneStep: true, // add done css
        //         enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        //     },
        //     contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
        //     disabledSteps: [], // Array Steps disabled
        //     errorSteps: [], // Highlight step with errors
        //     theme: 'dots',
        //     transitionEffect: 'fade', // Effect on navigation, none/slide/fade
        //     transitionSpeed: '400',
        //     onLeaveStep: leaveAStepCallback,
        // });
        $('#smartwizard').smartWizard({
            // Properties
            selected: 0, // Selected Step, 0 = first step   
            keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
            enableAllSteps: false, // Enable/Disable all steps on first load
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
            contentURL: null, // specifying content url enables ajax content loading
            contentURLData: null, // override ajax query parameters
            contentCache: true, // cache step contents, if false content is fetched always from ajax url
            cycleSteps: false, // cycle step navigation
            enableFinishButton: false, // makes finish button enabled always
            hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead
            errorSteps: [], // array of step numbers to highlighting as error steps
            labelNext: 'Next', // label for Next button
            labelPrevious: 'Previous', // label for Previous button
            labelFinish: 'Finish', // label for Finish button        
            noForwardJumping: false,
            // Events
            onLeaveStep: leaveAStepCallback, // triggers when leaving a step
            onShowStep: null, // triggers when showing a step
            onFinish: null // triggers when Finish button is clicked
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
        $("#wizard").smartWizard("fixHeight", 'none');
        // Init datepicker
        $('#datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate: new Date(new Date().setDate(new Date().getDate() + 0))
        });
    });

    function leaveAStepCallback(obj, context) {
        alert("Leaving step " + context.fromStep + " to go to step " + context.toStep);
        // return false to stay on step and true to continue navigation
        var valid = true;
        var step;
        if (context.fromStep == 1) {
            valid = valid_step1();
        } else if (context.fromStep == 2) {
            valid = valid_step2();
        }
        console.log(valid);
        if (valid) {
            return true;
        } else {
            return false;
        }
    }
</script>