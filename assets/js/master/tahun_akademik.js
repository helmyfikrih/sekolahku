function clearData() {
    jQuery("#txt_id").val("");
    jQuery("#username").val("");
    jQuery("#password").val("");
    jQuery("#name").val("");
    jQuery("#email").val("");
    jQuery("#id_region").html("");
    jQuery("#iregion").html("");
    jQuery("#id_branch").html("");
    jQuery("#ibranch").html("");
    jQuery("#id_subbranch").html("");
    jQuery("#isubbranch").html("");
    jQuery("#id_salesman").html("");
    jQuery("#isalesman").html("");
    //	jQuery("#role").val("");
    // jQuery("#imenu").html("");
    $('input:checkbox').removeAttr('checked');
    $("#accordion3 input:checkbox").prop("checked", false);
}

function loadTable() {
    $("#datatable-responsive").dataTable().fnDestroy();
    return $('#datatable-responsive').DataTable({
        "scrollX": true,
        "processing": false,
        "serverSide": true,
        "order": [],

        "ajax": {
            "url": "tahun_akademik/getTahunAkademik",
            "type": "POST",
            // "data": {
            //     "user_id": 451,
            //     "rayon": "asas",
            //     "test": "test",
            // }
        },

        "columnDefs": [{
            "targets": [0, 4],
            "orderable": false,
        },],

    });
}